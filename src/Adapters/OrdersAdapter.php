<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Crocus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus\Adapters;


use Crocus\Client;
use Crocus\Models\Orders;

class OrdersAdapter
{
    /**
     * @var Orders $data
     * */
    protected $data;
    /**
     * @var Client|null $client
     * */
    protected $client;

    /**
     * @param Orders $data
     * @param Client $client
     * @return void
     * */
    public function __construct(Orders $data, Client $client = null)
    {
        $this->data = $data;
        $this->client = $client;
    }

    /** id order
     * @override
     * @return int
     * */
    public function getOrderId()
    {
        return $this->data->id;
    }

    /** date_created order
     * @override
     * @return string
     * */
    public function getDateCreated()
    {
        return $this->data->date_created;
    }

    /** client_first_name client
     * @override
     * @return string
     * */
    public function getFirstName()
    {
        return $this->data->client_first_name;
    }

    /** client_second_name client
     * @override
     * @return string
     * */
    public function getSecondName()
    {
        return $this->data->client_second_name;
    }

    /** client_last_name client
     * @override
     * @return string
     * */
    public function getLastName()
    {
        return $this->data->client_last_name;
    }

    /** full_name client
     * @override
     * @param bool $withSecondName
     * @return string
     * */
    public function getFullName(bool $withSecondName = true)
    {
        return "{$this->getLastName()} {$this->getFirstName()}" . ($withSecondName ? " {$this->getSecondName()}" : '');
    }

    /** email client
     * @override
     * @return string
     * */
    public function getEmail()
    {
        return $this->data->email;
    }

    /** phone client
     * @override
     * @return string
     * */
    public function getPhone()
    {
        return $this->data->phone;
    }

    /** delivery
     * @override
     * @return string
     * */
    public function getDeliveryId()
    {
        return $this->data->delivery_option['id'] ?? null;
    }

    /** delivery
     * @override
     * @return string
     * */
    public function getDeliveryType()
    {
        return $this->data->delivery_option['name'] ?? null;
    }

    /** delivery
     * @override
     * @return string|null
     * */
    public function getDeliveryAddress()
    {
        return $this->data->delivery_address ?? null;
    }

    /** payment
     * @override
     * @return string
     * */
    public function getPaymentId()
    {
        return $this->data->payment_option['id'] ?? null;
    }

    /** payment
     * @override
     * @return string
     * */
    public function getPaymentType()
    {
        return $this->data->payment_option['name'] ?? null;
    }

    /** price
     * @override
     * @return string
     * */
    public function getPrice()
    {
        return $this->data->price;
    }

    /** comment notes
     * @override
     * @return string
     * */
    public function getComment()
    {
        return $this->data->client_notes;
    }

    /** products
     * @override
     * @return \Illuminate\Support\Collection
     * */
    public function getProducts()
    {
        return collect(array_map(function ($item) {
            return new OrdersProductAdapter($item, $this->client);
        }, $this->data->products ?? []));
    }

    /** status
     * @override
     * @return string
     * */
    public function getStatus()
    {
        return $this->data->status;
    }

    /** source
     * @override
     * @return string
     * */
    public function getSource()
    {
        return $this->data->source;
    }

    /**
     * @return Orders
     * */
    public function getOrder()
    {
        return $this->data;
    }

}
