<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Crocus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus\Adapters;


use Crocus\Client;

class OrdersProductAdapter
{
    /**
     * @var array $data
     * */
    protected $data;
    /**
     * @var Client|null $client
     * */
    protected $client;

    /**
     * @param array $data
     * @param Client $client
     * @return void
     * */
    public function __construct(array $data, Client $client = null)
    {
        $this->data = $data;
        $this->client = $client;
    }

    /** id
     * @override
     * @return string
     * */
    public function getId()
    {
        return $this->data['id'];
    }

    /** name
     * @override
     * @return string
     * */
    public function getName()
    {
        return $this->data['name'];
    }

    /** price
     * @override
     * @return string
     * */
    public function getPrice()
    {
        return $this->data['price'];
    }

    /** quantity
     * @override
     * @return string
     * */
    public function getQuantity()
    {
        return $this->data['quantity'];
    }

    /** total_price
     * @override
     * @return string
     * */
    public function getTotalPrice()
    {
        return $this->data['total_price'];
    }

    /** url
     * @override
     * @return string
     * */
    public function getLink()
    {
        return $this->data['url'];
    }

    /** measure_unit (шт. упаковка.)
     * @override
     * @return string
     * */
    public function getMeasureUnit()
    {
        return $this->data['measure_unit'];
    }

    /** article
     * @override
     * @return string
     * */
    public function getArticle()
    {
        return $this->data['sku'];
    }

}