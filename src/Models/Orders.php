<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Crocus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus\Models;

use Crocus\Adapters\OrdersAdapter;
use Crocus\Models\Helpers\HasFind;
use Crocus\Models\Helpers\HasList;

/**
 * @property int $id
 * @property string $date_created
 * @property string $client_first_name
 * @property string $client_second_name
 * @property string $client_last_name
 * @property string $client_id
 * @property string $email
 * @property string $phone
 * @property array $delivery_option ['id' => int, 'name' => string]
 * @property string $delivery_address
 * @property array $payment_option ['id' => int, 'name' => string]
 * @property string $price
 * @property string $client_notes
 * @property array $products
 * @property string $status
 * @property string $source
 * @property string $price_with_special_offer
 * @property string $special_offer_discount
 * @property string $special_offer_promocode
 *
 * @method $this status(string $status)
 * @method $this dateFrom(string $date)
 * @method $this dateTo(string $date)
 * @method $this limit(int $count)
 * @method $this lastId(int $id)
 * */
final class Orders extends Model
{
    use HasList, HasFind;

    protected $keyList = ['orders'];
    protected $keyFind = ['order'];

    /**
     * @param string $status
     * @param array $data
     * @return bool
     */
    public function setStatus(string $status, array $data = [])
    {
        if (empty($this->id)) {
            return false;
        }

        $data['status'] = $status;
        $data['ids'] = [$this->id];
        $result = $this->httpClient->post($this->getEntry() . '/set_status', ['json' => $data]);

        return isset($result['processed_ids']);
    }

    /**
     * @param \Illuminate\Support\Collection $collection
     * @param string $status
     * @param array $data
     * @return void
     */
    public function setStatuses($collection, string $status, array $data = [])
    {
        $collection->each(function ($one) use ($status, $data) {
            if (is_numeric($one)) {
                $id = $one;
            } elseif ($one instanceof self) {
                $id = $one->id;
            } elseif ($one instanceof OrdersAdapter) {
                $id = $one->getOrderId();
            }

            if (empty($id)) {
                return;
            }

            $order = new self($this->httpClient, ['id' => (int)$id]);
            $order->setStatus($status, $data);
        });
    }
}
