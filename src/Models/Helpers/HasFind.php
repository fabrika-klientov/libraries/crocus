<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Crocus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus\Models\Helpers;


trait HasFind
{
    /**
     * @param int $id
     * @return static
     * */
    public function find(int $id): self
    {
        $result = $this->httpClient->get($this->getEntry() . '/' . $id, []);

        return new static($this->httpClient, $this->getFindHelper($result ?? []));
    }

    /** helper
     * @param array $data
     * @return array
     * */
    protected function getFindHelper(array $data)
    {
        return empty($this->keyFind)
            ? []
            : collect($this->keyFind)->reduce(function ($result, $one) {
                return $result[$one] ?? [];
            }, $data);
    }
}