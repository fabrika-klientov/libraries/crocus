<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Crocus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus\Models\Helpers;


use Crocus\Core\Collection\Collection;

trait HasList
{
    /**
     * @return Collection
     * */
    public function get()
    {
        $options = $this->builder->getResult();
        $result = $this->httpClient->get($this->getEntry() . '/list', ['query' => $options]);

        return new Collection(array_map(function ($item) {
            return new self($this->httpClient, $item);
        }, $this->getListHelper($result ?? [])));
    }

    /** helper
     * @param array $data
     * @return array
     * */
    protected function getListHelper(array $data)
    {
        return empty($this->keyList)
            ? []
            : collect($this->keyList)->reduce(function ($result, $one) {
                return $result[$one] ?? [];
            }, $data);
    }
}