<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Crocus
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus\Models;


use Crocus\Core\Build\Builder;
use Crocus\Core\Helpers\Base;
use Crocus\Core\Query\HttpClient;
use Illuminate\Support\Str;

abstract class Model extends Base
{
    /**
     * @var HttpClient $httpClient
     * */
    protected $httpClient;
    /**
     * @var Builder $builder
     * */
    protected $builder;
    /**
     * @var array $keyList
     * */
    protected $keyList;
    /**
     * @var array $keyFind
     * */
    protected $keyFind;

    public function __construct(HttpClient $client, array $data = [])
    {
        parent::__construct($data);
        $this->httpClient = $client;
        $this->builder = new Builder();
    }

    /** magic where closure
     * @param string $name
     * @param array $arguments
     * @return $this
     * */
    public function __call($name, $arguments)
    {
        $this->builder->where(Str::snake($name), ...$arguments);

        return $this;
    }

    /** small entry partition
     * @return string
     * */
    protected function getEntry()
    {
        return Str::lower(last(explode('\\', get_class($this))));
    }

}
