<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Crocus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus\Models;


use Crocus\Models\Helpers\HasList;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property int $parent_group_id
 *
 * @method $this limit(int $count)
 * @method $this lastId(int $id)
 * */
class Groups extends Model
{
    use HasList;

    protected $keyList = ['groups'];
}