<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Crocus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus\Models;


use Crocus\Models\Helpers\HasFind;
use Crocus\Models\Helpers\HasList;

/**
 * @property int $id
 * @property string $external_id
 * @property string $name
 * @property string $sku
 * @property string $keywords
 * @property string $presence
 * @property bool $presence_sure
 * @property float $price
 * @property string $minimum_order_quantity
 * @property string $discount
 * @property array $prices
 * @property string $currency
 * @property string $description
 * @property array $group ['id' => int, 'name' => string]
 * @property array $category ['id' => int, 'caption' => string]
 * @property string $main_image
 * @property array $images
 * @property string $selling_type
 * @property string $status
 *
 * @method $this limit(int $count)
 * @method $this lastId(int $id)
 * @method $this groupId(int $id)
 * */
final class Products extends Model
{
    use HasList, HasFind;

    protected $keyList = ['products'];
    protected $keyFind = ['product'];
}