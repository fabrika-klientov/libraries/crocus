<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Crocus
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus\Core\Helpers;


use Illuminate\Support\Str;

/**
 * @property-read \Crocus\Models\Orders $orders
 * @property-read \Crocus\Models\Products $products
 * @property-read \Crocus\Models\Groups $groups
 *
 * */
trait Instances
{

    /** getter magic instances
     * @param string $name
     * @return \Crocus\Models\Model|null
     * */
    public function __get($name)
    {
        switch ($name) {
            case 'orders':
            case 'products':
            case 'groups':
                $class = 'Crocus\\Models\\' . Str::ucfirst($name);
                return new $class($this->httpClient);
        }

        return null;
    }
    
}