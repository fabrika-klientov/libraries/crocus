<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Crocus
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus\Core\Query;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class HttpClient
{
    /**
     * @var array $authData
     * */
    private $authData;
    /**
     * @var string $LINK
     * */
    private static $LINK = 'https://my.prom.ua/api/v1/';

    /**
     * @param array $auth
     * @return void
     * */
    public function __construct(array $auth)
    {
        $this->authData = $auth;
    }

    /** api GET
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws \Exception
     * */
    public function get($link, $options)
    {
        return $this->request('GET', $link, $options);
    }

    /** api POST
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws \Exception
     * */
    public function post($link, $options)
    {
        return $this->request('POST', $link, $options);
    }

    /** auth data
     * @return array
     * */
    public function getAuth()
    {
        return $this->authData;
    }

    /** full url
     * @param string $link
     * @return string
     * */
    public function getURL(string $link = '')
    {
        return self::$LINK . $link;
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws \Exception
     * */
    protected function request(string $method, string $link, array $options)
    {
        try {
            $startTime = time();

            $response = $this->getClient()->request($method, $link, $options);
            $resData = $response->getBody()->getContents();
            $result = json_decode($resData, true);

            $this->logging("Request:: [$method] [$link]", [
                'lib' => 'crocus',
                'lib_user' => $this->authData['token'] ?? '',
                'request' => $options,
                'request_url' => $link,
                'request_headers' => $options['headers'] ?? [],
                'request_method' => $method,
                'response' => substr($resData, 0, 20000),
                'response_code' => $response->getStatusCode(),
                'response_headers' => $response->getHeaders(),
                'execute_time' => time() - $startTime,
                'request_content_length' => strlen((string)json_encode($options)),
                'response_content_length' => strlen($resData),
            ]);

            if (!empty($result)) {
                return $result;
            }
        } catch (ClientException $clientException) {
            throw new \Exception('Request returned error. ' . $clientException->getMessage(), $clientException->getCode());
        } catch (\Exception $exception) {
            throw new \Exception('Request returned error. ' . $exception->getMessage());
        }

        return null;
    }

    /**
     * @return Client
     * */
    protected function getClient()
    {
        return new Client([
            'base_uri' => self::$LINK,
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => "Bearer {$this->authData['token']}"
            ],
        ]);
    }

    /** logging
     * @param string $message
     * @param array $context
     * @return void
     * */
    protected function logging($message, $context)
    {
        $logger = '\Log';
        if (class_exists($logger)) {
            $message = 'CROCUS // [' . ($this->authData['token'] ?? '') . '] ' . $message;
            try {
                $logger::channel('another')->info($message, $context);
            } catch (\Exception $exception) {
                $logger::info($message, $context);
            }
        }
    }

}
