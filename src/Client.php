<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Crocus
 * @category  Crocus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Crocus;


use Crocus\Core\Helpers\Instances;
use Crocus\Core\Query\HttpClient;

class Client
{
    use Instances;

    /**
     * @var HttpClient $httpClient
     * */
    private $httpClient;

    /**
     * @param array $auth ['token' => string]
     * @return void
     * */
    public function __construct(array $auth)
    {
        if ($this->validateAuth($auth)) {
            $this->httpClient = new HttpClient($auth);
        }
    }

    /**
     * @return HttpClient
     * */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /** validator for auth data
     * @param array $auth
     * @return bool
     * @throws \Exception
     * */
    protected function validateAuth(array $auth)
    {
        if (empty($auth)) {
            throw new \Exception('Auth params is empty');
        }

        if (!empty($auth['token']) && is_string($auth['token'])) {
            return true;
        }

        throw new \Exception('Auth params [token] is required');
    }
}