## Status

[![Latest Stable Version](https://poser.pugx.org/shadoll/crocus/v/stable)](https://packagist.org/packages/shadoll/crocus)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/crocus/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/crocus/commits/master)
[![coverage report](https://gitlab.com/fabrika-klientov/libraries/crocus/badges/master/coverage.svg)](https://gitlab.com/fabrika-klientov/libraries/crocus/commits/master)
[![License](https://poser.pugx.org/shadoll/crocus/license)](https://packagist.org/packages/shadoll/crocus)

**[Prom](https://prom.ua/) PHP API Library**

---


## Install

`composer require shadoll/crocus`


### Использование 

**Не все методы реализованны**

```php

$client = new \Crocus\Client([
    'token' => 'token...',
]);

// Коллекция заказов
/**
 * @var \Crocus\Core\Collection\Collection<\Crocus\Models\Orders> $collect
 **/
$collect = $client->orders->get();
// с фильтрами (больше фильтров смотрите в классе модели)
$collect = $client->orders->dateFrom('2019-12-24T00:00:00')->status(123)->get();

// Модель по ID
/**
 * @var \Crocus\Models\Orders $model
 **/
$model = $client->orders->find(96421289);

// Коллекция групп (больше фильтров смотрите в классе модели)
$collect = $client->groups->limit(100)->get();

// Коллекция продуктов
/**
 * @var \Crocus\Core\Collection\Collection<\Crocus\Models\Products> $collect
 **/
$collect = $client->products->get();
// с фильтрами (больше фильтров смотрите в классе модели)
$collect = $client->products->groupId(16742130)->limit(10)->get();

// Модель по ID
/**
 * @var \Crocus\Models\Products $model
 **/
$model = $client->products->find(12345);


```

**_Количество моделей и функций будут пополнятся_**