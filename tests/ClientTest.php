<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 24.12.19
 * Time: 21:56
 */

namespace Tests;

use Crocus\Client;
use Crocus\Core\Query\HttpClient;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{

    public function test__construct()
    {
        $client = new Client(['token' => 'any-string']);
        $this->assertInstanceOf(Client::class, $client);

        $this->expectException(\Exception::class);
        new Client([]);
    }

    public function testGetHttpClient()
    {
        $client = new Client(['token' => 'any-string']);

        $this->assertInstanceOf(HttpClient::class, $client->getHttpClient());
    }
}
